package com.salary.employee.service;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;



@SpringBootTest
public class EmployeeServiceTests {
	
	@Test
	void calcAnnualSalaryByHourlyTest() {
		// Arrange
		EmployeeService employeeService = new EmployeeService();
		
		// Action
		
		float response = employeeService.calcAnnualSalary(10, 30, "HourlySalaryEmployee");
		
		// Assert
		Assert.isTrue(response == 14400, "The calculation is made with the hourly salary");
		
	}
	
	@Test
	void calcAnnualSalaryByMonthlyTest() {
		// Arrange
		EmployeeService employeeService = new EmployeeService();
		
		// Action
		
		float response = employeeService.calcAnnualSalary(10, 30, "MonthlySalaryEmployee");
		
		// Assert
		Assert.isTrue(response == 360, "The calculation is made with the monthly salary");
		
	}
	
	@Test
	void calcAnnualSalaryWhitoutContractTypeTest() {
		// Arrange
		EmployeeService employeeService = new EmployeeService();
		
		// Action
		
		float response = employeeService.calcAnnualSalary(10, 30, null);
		
		// Assert
		Assert.isTrue(response == 0, "Response is zero because the contract type value is necessary");
		
	}

}
