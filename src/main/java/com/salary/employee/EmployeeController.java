package com.salary.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.salary.employee.model.Response;
import com.salary.employee.service.EmployeeService;

@RestController
public class EmployeeController {
	
	@Autowired
	EmployeeService employeeService;
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/employees")
	public Response getAllEmployees() {
		return employeeService.getAllEmployees();
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/employees/{employeeId}")
	public Response getEmployeeById(@PathVariable("employeeId") int employeeId) {
		return employeeService.getEmployeeById(employeeId);
	}
}
