package com.salary.employee.model;

import java.util.List;

public class Response {
	private List<EmployeeResponse> employees;
	private EmployeeResponse employee;
	private String errorMessage;
	
	public Response(List<EmployeeResponse> employees, EmployeeResponse employee, String errorMessage) {
		super();
		this.employees = employees;
		this.employee = employee;
		this.errorMessage = errorMessage;
	}

	public List<EmployeeResponse> getEmployees() {
		return employees;
	}

	public void setEmployees(List<EmployeeResponse> employees) {
		this.employees = employees;
	}

	public EmployeeResponse getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeResponse employee) {
		this.employee = employee;
	}

	public String geterrorMessage() {
		return errorMessage;
	}

	public void seterrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
