package com.salary.employee.model;

public class EmployeeResponse {
	
	private int id;
	private String name;
	private String contractType;
	private String roleName;
	private String roleDescription;
	private float annualSalary;
	public EmployeeResponse(int id, String name, String contractType, String roleName, String roleDescription,
			float annualSalary) {
		super();
		this.id = id;
		this.name = name;
		this.contractType = contractType;
		this.roleName = roleName;
		this.roleDescription = roleDescription;
		this.annualSalary = annualSalary;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getRoleDescription() {
		return roleDescription;
	}
	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}
	public float getAnnualSalary() {
		return annualSalary;
	}
	public void setAnnualSalary(float annualSalary) {
		this.annualSalary = annualSalary;
	}
}
