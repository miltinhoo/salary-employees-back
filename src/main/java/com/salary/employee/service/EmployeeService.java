package com.salary.employee.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.salary.employee.model.Employee;
import com.salary.employee.model.EmployeeResponse;
import com.salary.employee.model.Response;

@Service
public class EmployeeService {	
	
	private final String HOURLY_SALARY = "HourlySalaryEmployee";
	private final String NO_DATA_FOUND = "No data found";
	
	public Response getAllEmployees() {
		List<Employee> employees = getEmployeesFromApi();
		if(employees.size() == 0) { return getResponse(null, null, NO_DATA_FOUND); }
		
		List<EmployeeResponse> employeesResponse = employees.stream()
								.map(emp -> getEmployeeRespFromEmploye(emp) ).collect(Collectors.toList());		
		
		return getResponse(employeesResponse, null, null);
	}
	
	public Response getEmployeeById(int employeeId) {
		List<Employee> employees = getEmployeesFromApi();
		if(employees.size() == 0) { return getResponse(null, null, NO_DATA_FOUND); }

		try {
			Optional<Employee> employeeOption = employees.stream().filter(emp -> emp.getId() == employeeId).findFirst();
			return getResponse(null, getEmployeeRespFromEmploye(employeeOption.get()), null);
		} catch (Exception e) {
			return getResponse(null, null, NO_DATA_FOUND);
		}		
		
	}
	
	private List<Employee> getEmployeesFromApi() {
		final String uri = "http://masglobaltestapi.azurewebsites.net/api/employees/";

		try {
			RestTemplate restTemplate = new RestTemplate();
			String result = restTemplate.getForObject(uri, String.class);
			return stringToArray(result, Employee[].class);
		} catch (Exception e) {
			return new ArrayList<Employee>();
		}
	}
	
	private EmployeeResponse getEmployeeRespFromEmploye(Employee employee) {
		if(employee == null) { return null; }
		return new EmployeeResponse(employee.getId(), employee.getName(), employee.getContractTypeName(), employee.getRoleName(), employee.getRoleDescription(), calcAnnualSalary(employee.getHourlySalary(), employee.getMonthlySalary(), employee.getContractTypeName()));
	}
	
	protected float calcAnnualSalary(float hourlySalary, float monthlySalary, String contractType) {
		if(contractType == null) { return 0; }
		return contractType.equalsIgnoreCase(HOURLY_SALARY) ? 120 * hourlySalary * 12 : monthlySalary * 12;
	}

	private <T> List<T> stringToArray(String value, Class<T[]> convertClass) {
		T[] newArray = new Gson().fromJson(value, convertClass);
		return Arrays.asList(newArray);
	}
	
	private Response getResponse(List<EmployeeResponse> employeesResponse, EmployeeResponse employeeResponse, String error) {
		return new Response(employeesResponse, employeeResponse, error);
	}

}
